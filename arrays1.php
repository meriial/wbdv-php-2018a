<pre>
<?php

$a = [
    'cat', 'dog', 'cow'
];

array_push($a, 'duck');
$a[] = 'mouse'; // same thing as array_push($a, 'thing');

// $joined = implode('|', $a);
// array_pop() -- takes element off end
// array_shift() -- takes element off front
// array_unshift() -- puts element on front
// array_push() -- puts element on end
// count() -- number of elements in the array

// sort($a);

// var_dump(count($a));

echo $a[0];
