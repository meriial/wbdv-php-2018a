<?php

    require_once 'models/User.php';
    require_once 'models/Tweet.php';

    $user1 = new User();
    $user1->name = 'Donald Trump';
    $user1->handle = 'realDonaldTrump';
    $user1->image = 'https://pbs.twimg.com/profile_images/859982100904148992/hv5soju7_bigger.jpg';
    $user1->dateJoined = date_create();

    var_dump($user1); die;

    $user2 = new User();
    $user2->name = 'Barack Obama';
    $user2->handle = 'BarackObama';
    $user2->image = 'https://pbs.twimg.com/profile_images/822547732376207360/5g0FC8XX_bigger.jpg';
    $user2->dateJoined = date_create();

    // Method 1 to create a tweet
    $tweet1 = new Tweet();
    $tweet1->user = $user1;
    $tweet1->content = 'Content 1';
    $tweet1->time = '3h';

    array_push($tweet1->comments, 'Woohoo!');
    $tweet1->comments[] = 'Great!';

    $tweet2 = new Tweet();
    $tweet2->user = $user2;
    $tweet2->content = 'Content 2';
    $tweet2->time = '3h';

    // Method 2 involves making a constructor and passing in arguments.
    // $tweet1 = new Tweet($user, 'Content', '3h');

    // $tweet1 = [
    //     'user' => $user1,
    //     'time' => '2h',
    //     'comments' => [
    //         'Great!',
    //         'Out of this world!'
    //     ],
    //     'retweetCount' => '312',
    //     'likeCount' => '132',
    //     'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    // ];

    // $tweet2 = [
    //     'user' => $user2,
    //     'time' => '3h',
    //     'comments' => [
    //         'Great!',
    //         'Out of this world!',
    //         'This sux.'
    //     ],
    //     'retweetCount' => '321',
    //     'likeCount' => '13',
    //     'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    // ];

    $tweets = [$tweet1, $tweet2];
    $users = [$user1, $user2];

    include('views/body.view.php');
