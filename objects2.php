<pre>
<?php

class Cake {
    public $flavour = 'vanilla';
    public $shape = 'round';

    public function bake($temp)
    {
        echo "Baking at $temp degrees.\n";
    }

    public function isReady()
    {
        $this->taste();
        // var_dump($this->flavour);
        echo "Your $this->flavour cake is ready.";
    }

    private function taste()
    {
        echo "Yummm\n";
    }
}

$cake1 = new Cake();
var_dump($cake1->flavour);

$cake1->flavour = 'chocolate';
// $cake1->shape = 'circle';
$cake1->bake(350);
$cake1->isReady();
// $cake1->taste();
