<pre>
<?php

class Foo {

}

$i = 1;
$j = 1.2;
$k = false;
$thing = 'thing';
$a = ['a', 1, true, 1.2];
$o = new stdClass();
$f = new Foo();

$func = function() {
    return 'hi';
};

$foo = 'bar';
$thing = 'thing2';

var_dump($i, $j, $k, $a, $o, $f, $func, $thing);
