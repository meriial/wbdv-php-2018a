<?php

    class User {
        public $name;
        public $handle;
        public $image;
        public $dateJoined;
    }

    class Tweet {
        public $user;
        public $time;
        public $content;
        public $comments = [];
        public $retweets = [];
        public $likes = [];
    }

    $user1 = [
        'name' => 'Donald Trump',
        'handle' => 'realDonaldTrum',
        'image' => 'https://pbs.twimg.com/profile_images/859982100904148992/hv5soju7_bigger.jpg',
        'dateJoined' => date_create()
    ];

    $user2 = [
        'name' => 'Barack Obama',
        'handle' => 'BarackObama',
        'image' => 'https://pbs.twimg.com/profile_images/822547732376207360/5g0FC8XX_bigger.jpg',
        'dateJoined' => date_create()->modify('-1 year')
    ];

    $tweet1 = [
        'user' => $user1,
        'time' => '2h',
        'comments' => [
            'Great!',
            'Out of this world!'
        ],
        'retweetCount' => '312',
        'likeCount' => '132',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    ];

    $tweet2 = [
        'user' => $user2,
        'time' => '3h',
        'comments' => [
            'Great!',
            'Out of this world!',
            'This sux.'
        ],
        'retweetCount' => '321',
        'likeCount' => '13',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    ];

    $tweets = [$tweet1, $tweet2];
    $users = [$user1, $user2];
?>
<h1>Twitter</h1>
<h2>Users</h2>
<ul>
    <?php foreach ($users as $user): ?>
        <li>
            <img src="<?php echo $user['image'] ?>" alt="">
            <?php echo $user['name'] ?><br/>
            @<?php echo $user['handle'] ?>
        </li>
    <?php endforeach; ?>
</ul>
<h2>Tweets</h2>
<ul>
    <?php foreach($tweets as $tweet): ?>
        <li>
            <img src="<?php echo $tweet['user']['image'] ?>" />
            <?php echo $tweet['user']['name'] ?>
            <?php echo $tweet['user']['handle'] ?>
            <?php echo $tweet['time'] ?>
            <?php echo $tweet['content'] ?>
            <?php echo $tweet['retweetCount'] ?>
            <?php echo count($tweet['comments']) ?>
            <?php echo $tweet['likeCount'] ?>
        </li>
    <?php endforeach; ?>
</ul>
