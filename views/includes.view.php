<h1>Twitter</h1>
<h2>Users</h2>
<ul>
    <?php foreach ($users as $user): ?>
        <li>
            <img src="<?php echo $user->image ?>" alt="">
            <?php echo $user->name ?><br/>
            @<?php echo $user->handle ?>
        </li>
    <?php endforeach; ?>
</ul>
<h2>Tweets</h2>
<ul>
    <?php foreach($tweets as $tweet): ?>
        <li>
            <?php include 'views/tweet.view.php' ?>
        </li>
    <?php endforeach; ?>
</ul>
