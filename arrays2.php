<pre>
<?php

$bob = [
    'name' => 'Bob',
    'eyes' => 'blue',
    'hair' => 'brown',
    'height' => 'tall'
];

$jane = [
    'name' => 'Jane',
    'eyes' => 'hazel',
    'hair' => 'green',
    'height' => 'average'
];

$bob['money'] = 'rich';
$jane['money'] = 'billions';

$people = [$bob, $jane];

// $json = json_encode($people, JSON_PRETTY_PRINT);
// var_dump($json, $people);

echo $people[0]['name']."\n";
echo $people[0]['eyes']."\n";
echo $people[0]['hair']."\n";
echo $people[0]['height']."\n";
echo $people[0]['money']."\n";

echo $people[1]['name']."\n";
echo $people[1]['eyes']."\n";
echo $people[1]['hair']."\n";
echo $people[1]['height']."\n";
echo $people[1]['money']."\n";

// I can replace all of this with loops.
echo "\n\n\n\n";

foreach ($people as $person) {
    foreach ($person as $trait => $value) {
        echo $trait . ": ". $value."\n";
    }
}
