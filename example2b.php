<?php

    class User {
        public $name;
        public $handle;
        public $image;
        public $dateJoined;
    }

    class Tweet {
        public $user;
        public $time;
        public $content;
        public $comments = [];
        public $retweets = [];
        public $likes = [];

        public function __construct($user, $content, $time)
        {
            $this->user = $user;
            $this->content = $content;
            $this->time = $time;
        }
    }

    $user1 = new User();
    $user1->name = 'Donald Trump';
    $user1->handle = 'realDonaldTrump';
    $user1->image = 'https://pbs.twimg.com/profile_images/859982100904148992/hv5soju7_bigger.jpg';
    $user1->dateJoined = date_create();

    $user2 = new User();
    $user2->name = 'Barack Obama';
    $user2->handle = 'BarackObama';
    $user2->image = 'https://pbs.twimg.com/profile_images/822547732376207360/5g0FC8XX_bigger.jpg';
    $user2->dateJoined = date_create();

    // Method 2 to create a tweet
    $tweet1 = new Tweet($user1, 'Content1', '2h');
    $tweet2 = new Tweet($user2, 'Content2', '3h');

    // Method 2 involves making a constructor and passing in arguments.
    // $tweet1 = new Tweet($user, 'Content', '3h');

    // $tweet1 = [
    //     'user' => $user1,
    //     'time' => '2h',
    //     'comments' => [
    //         'Great!',
    //         'Out of this world!'
    //     ],
    //     'retweetCount' => '312',
    //     'likeCount' => '132',
    //     'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    // ];

    // $tweet2 = [
    //     'user' => $user2,
    //     'time' => '3h',
    //     'comments' => [
    //         'Great!',
    //         'Out of this world!',
    //         'This sux.'
    //     ],
    //     'retweetCount' => '321',
    //     'likeCount' => '13',
    //     'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
    // ];

    $tweets = [$tweet1, $tweet2];
    $users = [$user1, $user2];
?>
<h1>Twitter</h1>
<h2>Users</h2>
<ul>
    <?php foreach ($users as $user): ?>
        <li>
            <img src="<?php echo $user->image ?>" alt="">
            <?php echo $user->name ?><br/>
            @<?php echo $user->handle ?>
        </li>
    <?php endforeach; ?>
</ul>
<h2>Tweets</h2>
<ul>
    <?php foreach($tweets as $tweet): ?>
        <li>
            <img src="<?php echo $tweet->user->image ?>" />
            <?php echo $tweet->user->name ?>
            <?php echo $tweet->user->handle ?>
            <?php echo $tweet->time ?>
            <?php echo $tweet->content ?>
            <?php echo count($tweet->retweets) ?>
            <?php echo count($tweet->comments) ?>
            <?php echo count($tweet->likes) ?>
        </li>
    <?php endforeach; ?>
</ul>
