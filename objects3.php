<pre>
<?php

class Cake {

    public $flavour;

    public function __construct($flavour)
    {
        echo "In constructor!\n";
        $this->flavour = $flavour;
    }

}

$cake1 = new Cake('chocolate');

var_dump($cake1);
