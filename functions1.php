<pre>
<?php

// $thing = 'Phyllis';

function sayHelloTo($name) {
    // $otherThing = 'other';
    // echo $thing."\n";
    $output = 'Hello!! How are you doing, ' . $name . "\n";

    return $output;
}

$bob = 'Bob';
$bobsGreeting = sayHelloTo($bob);

function echoPage($bobsGreeting) {
    echo "-------\n" . sayHelloTo('Bob') . '-------'. "\n";
    echo "-------\n" . $bobsGreeting . '-------'. "\n";


    echo sayHelloTo('John');
    echo sayHelloTo('Jane');
}

echoPage($bobsGreeting);
// echo $otherThing;
